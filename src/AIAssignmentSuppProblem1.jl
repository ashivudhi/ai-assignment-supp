### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ d16dad80-e25e-11eb-08d9-5fce721c42df
md"# Genetic Algorithm"

# ╔═╡ a7ea60b0-c5a8-49f4-8c42-5e4d50b98198
md"##### Initial Population Implementation"

# ╔═╡ aaeee551-a860-44c9-8967-41d10f198761
function generate_population(size)
	initialPopulationWeights = []
	for i in 1:size
		push!(initialPopulationWeights, rand()*2)
	end
	return initialPopulationWeights
end

# ╔═╡ db9d4ff1-9132-4b40-8408-d477e86350e3
initialPopulationWeights = generate_population(20)

# ╔═╡ 6c7410a7-0b4e-41c5-b63e-f64e2bb0f660
md"##### Fitness Function"

# ╔═╡ d2f4e3f1-31ce-4b99-9779-3e885df28bfe
#Measuring the fitness of each chromosome in the population
function calculate_fitness(initialPopulationWeights)
	for i in eachindex(initialPopulationWeights)
		if initialPopulationWeights[i] > 1.5
			return fitpop = initialPopulationWeights[initialPopulationWeights .> 1.5]
		end
	end
end

# ╔═╡ 9a0b347f-1cc3-47c8-9fb2-22556323c5ca


# ╔═╡ 877c8b5c-fad4-4903-8632-f1f5734366a6
fitness = calculate_fitness(initialPopulationWeights)

# ╔═╡ 0463c8dd-6137-4e72-bd65-b3805305abc2
md"##### Selection Probability"

# ╔═╡ 5e31a967-7199-45ff-8093-e14ebb705410
function selection(parents,fitness)
	for parent_num in 1:parents
        max_fitness = max(fitness)
        parents,parent_num = max_fitness
	end
    return parents
	end

# ╔═╡ 892bdd6c-668a-42ab-9195-99f00163cb58
md"##### Crossover Point"

# ╔═╡ 9d52a8de-00b0-4ed3-b04a-d58a05666981
function cross_over(parents, offspring_size)
	offspring_partA = parents[1:crossover_point]
	offspring_partB = parents[crossover_point:end]
	return vcat(offspring_partA,offspring_partB)
end

# ╔═╡ 2afa7fb9-6f9a-4e6b-bce8-775328eb254f
md"##### Mutation Probability"

# ╔═╡ 3221e219-166a-46eb-b1b5-749118d4e819
#Adding some variations to the offsprings
function mutate(offspring_crossover, mutation_probability)
	new= map(x->round.(x), child)
	last = new[end,:]
	for i in eachindex(new)
		if i < mutation_probability
			i, last = last, i
			return new
		end
	end
end

# ╔═╡ 02e63b7e-365d-42cc-82fd-cebd9c51d3c6
md"##### Algorithm Execution"

# ╔═╡ e8931528-9e27-4989-8c33-7004b1946625
genetic_algorithm(initialPopulationWeights, calculate_fitness, )

# ╔═╡ d77fe846-4c5d-483b-ae77-b550fab9848c


# ╔═╡ 01fa6642-d096-4966-b596-4fe74778a0ba


# ╔═╡ 9ef94078-971d-4156-9bef-a56f348f9cd1


# ╔═╡ 082e98da-d166-4aa0-9032-f4fbfd491a02


# ╔═╡ Cell order:
# ╠═d16dad80-e25e-11eb-08d9-5fce721c42df
# ╠═a7ea60b0-c5a8-49f4-8c42-5e4d50b98198
# ╠═aaeee551-a860-44c9-8967-41d10f198761
# ╠═db9d4ff1-9132-4b40-8408-d477e86350e3
# ╠═6c7410a7-0b4e-41c5-b63e-f64e2bb0f660
# ╠═d2f4e3f1-31ce-4b99-9779-3e885df28bfe
# ╠═9a0b347f-1cc3-47c8-9fb2-22556323c5ca
# ╠═877c8b5c-fad4-4903-8632-f1f5734366a6
# ╠═0463c8dd-6137-4e72-bd65-b3805305abc2
# ╠═5e31a967-7199-45ff-8093-e14ebb705410
# ╠═892bdd6c-668a-42ab-9195-99f00163cb58
# ╠═9d52a8de-00b0-4ed3-b04a-d58a05666981
# ╠═2afa7fb9-6f9a-4e6b-bce8-775328eb254f
# ╠═3221e219-166a-46eb-b1b5-749118d4e819
# ╠═02e63b7e-365d-42cc-82fd-cebd9c51d3c6
# ╠═e8931528-9e27-4989-8c33-7004b1946625
# ╠═d77fe846-4c5d-483b-ae77-b550fab9848c
# ╠═01fa6642-d096-4966-b596-4fe74778a0ba
# ╠═9ef94078-971d-4156-9bef-a56f348f9cd1
# ╠═082e98da-d166-4aa0-9032-f4fbfd491a02
