### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ da5dba3c-e2a1-11eb-2949-b31ea7f7bc64
#domains Definition
@enum VarDomain one two three four 

# ╔═╡ b5d223de-3224-4394-92b3-74a08294c41c
mutable struct ConstraintVar
	name::String
	value::Union{Nothing,VarDomain}
	restricted::Vector{VarDomain}
	restrictionNumber::Int64
end

# ╔═╡ 3b998fa6-78eb-4db0-9aae-dddba365573f
struct Variables
	variables::Vector{ConstraintVar}
	constraints::Vector{Tuple{ConstraintVar,ConstraintVar}}
end

# ╔═╡ 4973084d-38b4-4b46-a746-a0a7df60eac7
function cspSolution(pb::Variables, allAssignments)
	for currentVariable in pb.variables
		if currentVariable.restrictionNumber == 4
			return println("No variable to assign")
		else
			#randomly assigning values to the variables
			nextValue = rand(setdiff(Set([one,two,three,four]),
					Set(currentVariable.restricted)))
			currentVariable.value = nextValue
			
			#forward checking
			for currentConstraint in pb.constraints
				
				if !((currentConstraint[1] == currentVariable) || (currentConstraint[2] ==
currentVariable))
					continue
				else
					if currentConstraint[1] == currentVariable
						push!(currentConstraint[2].restricted, nextValue)
						currentConstraint[2].restrictionNumber += 1
					else
						push!(currentConstraint[1].restricted, nextValue)
						currentConstraint[1].restrictionNumber += 1
					end
				end
			end
			push!(allAssignments, currentVariable.name => nextValue)
		end
	end
	return allAssignments
end

# ╔═╡ 630343ef-b753-4195-a2a1-be28fe37e217
X1 = ConstraintVar("X1",nothing, [], 0)

# ╔═╡ 9f26079d-a495-4fc9-84c7-d1d56f9884d5
X2 = ConstraintVar("X2",nothing, [], 0)

# ╔═╡ bba1b38a-7c3b-40a3-8203-8371f447df2b
X3 = ConstraintVar("X3",one, [two,three,four], 3)

# ╔═╡ 3c37d8e3-3f10-4794-b192-1a85771f1a09
X4 = ConstraintVar("X4",nothing, [], 0)

# ╔═╡ ae44ff36-195b-4be5-9702-43f9afb81eda
X5 = ConstraintVar("X5",nothing, [], 0)

# ╔═╡ ec7d3d75-0ff9-4b3c-a2c1-200023fb1122
X6 = ConstraintVar("X6",nothing, [], 0)

# ╔═╡ f7ea6635-cd22-439f-9384-9890d5dbfc0e
X7 = ConstraintVar("X7",nothing, [], 0)

# ╔═╡ 3b72ebc7-5728-4529-a7ef-1d5d91f31681
CSProblem = Variables([X1, X2, X3, X4, X5, X6, X7], [(X1,X2), (X1,X3), (X1,X4),(X1,X5),(X2,X5),(X3,X4),(X4,X5),(X4,X6),(X5,X6),(X6,X7)])

# ╔═╡ 24585afc-1475-46f5-a41e-773469798a1f
cspSolution(CSProblem,[])

# ╔═╡ Cell order:
# ╠═da5dba3c-e2a1-11eb-2949-b31ea7f7bc64
# ╠═b5d223de-3224-4394-92b3-74a08294c41c
# ╠═3b998fa6-78eb-4db0-9aae-dddba365573f
# ╠═4973084d-38b4-4b46-a746-a0a7df60eac7
# ╠═630343ef-b753-4195-a2a1-be28fe37e217
# ╠═9f26079d-a495-4fc9-84c7-d1d56f9884d5
# ╠═bba1b38a-7c3b-40a3-8203-8371f447df2b
# ╠═3c37d8e3-3f10-4794-b192-1a85771f1a09
# ╠═ae44ff36-195b-4be5-9702-43f9afb81eda
# ╠═ec7d3d75-0ff9-4b3c-a2c1-200023fb1122
# ╠═f7ea6635-cd22-439f-9384-9890d5dbfc0e
# ╠═3b72ebc7-5728-4529-a7ef-1d5d91f31681
# ╠═24585afc-1475-46f5-a41e-773469798a1f
